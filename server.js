'use strict'

const hapi = require('hapi')
const server = new hapi.Server({port: 8080})

const assert = require('assert')

const essentialPlugins = require('config/essential-plugins.config')
const httpDevErrors = require('config/http-dev-errors.config')
const viewsConfig = require('config/views.config')

const extensions = require('extensions')
const routes = require('routes')

const cookieSchemeStrategy = require('strategies/cookie-scheme.strategy')

const loggerService = require('services/logger.service')
const dbService = require('services/db.service')

async function initServer() {
  await server.register(essentialPlugins())
  await server.register(httpDevErrors())
  server.auth.strategy('cookie', 'cookie', cookieSchemeStrategy)
  server.auth.default('cookie')
  server.ext(extensions)
  server.route(routes)
  server.views(viewsConfig())
  dbService.initialise()
  return server
}

async function stopServer() {
  try {
    loggerService.info('Initiating server shutdown')
    await server.stop()
  } catch (err) {
    loggerService.error(`Server failed to shutdown gracefully: ${err}`)
    assert(!err, err)
  }
}

module.exports = {
  initServer,
  stopServer
}
