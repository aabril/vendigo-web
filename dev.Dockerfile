FROM vendigo/web:latest

USER root

VOLUME /opt/app/node_modules

RUN apk add --no-cache tree &&\
  npm install -g nodemon &&\
  npm install

ENV NODE_ENV=development NODE_PATH=./lib:./test

EXPOSE 5858

USER node

CMD ["nodemon", "index.js"]
