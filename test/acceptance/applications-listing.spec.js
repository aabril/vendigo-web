'use strict'

const cheerio = require('cheerio')

const {initServer, stopServer, getTemplate} = require('support/server')

describe('GET /applications', () => {
  let server

  const request = {
    method: 'get',
    url: '/applications',
    credentials: {}
  }

  beforeAll(async () => {
    server = await initServer()
  })

  afterAll(async () => {
    await stopServer()
  })

  it('responds with 200 "OK" and html content type', async () => {
    const response = await server.inject(request)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toMatch(/^text\/html/)
  })

  it('responds with the "applications/index" template', async () => {
    const response = await server.inject(request)
    expect(getTemplate(response)).toBe('applications/listing')
  })

  it('renders applications list', async () => {
    const response = await server.inject(request)
    const $ = cheerio.load(response.result)
    expect($('.applications-list').length).toEqual(1)
  })
})
