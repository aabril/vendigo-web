'use strict'

const {initServer, stopServer} = require('support/server')

describe('/ home', () => {
  describe('GET /', () => {
    let server

    beforeAll(async () => {
      server = await initServer()
    })

    afterAll(async () => {
      await stopServer()
    })

    describe('when unauthenticated', () => {
      const request = {
        method: 'GET',
        url: '/'
      }

      it('redirects to /login', async () => {
        const response = await server.inject(request)
        expect(response.statusCode).toBe(302)
        expect(response.headers.location).toBe('/login')
      })
    })

    describe('when authenticated', () => {
      const request = {
        method: 'GET',
        url: '/',
        credentials: {}
      }
      it('redirects to /login', async () => {
        const response = await server.inject(request)
        expect(response.statusCode).toBe(307)
        expect(response.headers.location).toBe('/login')
      })
    })
  })
})
