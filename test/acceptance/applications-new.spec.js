'use strict'

const cheerio = require('cheerio')

const {initServer, stopServer, getTemplate} = require('support/server')

describe('GET /applications/new', () => {
  let server

  const request = {
    method: 'get',
    url: '/applications/new',
    credentials: {}
  }

  beforeAll(async () => {
    server = await initServer()
  })

  afterAll(async () => {
    await stopServer()
  })

  it('responds with 200 "OK" and html content type', async () => {
    const response = await server.inject(request)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toMatch(/^text\/html/)
  })

  it('responds with the "applications/new" template', async () => {
    const response = await server.inject(request)
    expect(getTemplate(response)).toBe('applications/new')
  })

  it('displays nav with "Applications" selected', async () => {
    const response = await server.inject(request)
    const $ = cheerio.load(response.result)
    expect($('.vf-primary-nav__list-item[selected] a').text()).toBe(
      'Applications'
    )
  })

  it('displays application step with "Payment Terms" selected', async () => {
    const response = await server.inject(request)
    const $ = cheerio.load(response.result)
    expect($('.vf-application-steps__step[selected] span').text()).toBe(
      'Payment Terms'
    )
  })

  it('displays form title', async () => {
    const response = await server.inject(request)
    const $ = cheerio.load(response.result)
    expect($('h1').text()).toBe('Create a new application')
  })

  it('displays payment-terms form', async () => {
    const response = await server.inject(request)
    const $ = cheerio.load(response.result)
    expect($('form[name="payment-terms"]').length).toBe(1)
  })

  describe('Purchase amount', () => {
    it('displays purchase amount label', async () => {
      const response = await server.inject(request)
      const $ = cheerio.load(response.result)
      expect($('label[for="purchaseAmount"]').text()).toBe('Purchase amount')
    })

    it('displays purchase amount input', async () => {
      const response = await server.inject(request)
      const $ = cheerio.load(response.result)
      const $purchaseAmount = $('input[name="purchaseAmount"]')

      expect($purchaseAmount.length).toBe(1)
      expect($purchaseAmount.attr('type')).toBe('number')
      expect($purchaseAmount.attr('placeholder')).toBe('eg. 2000')
      expect($purchaseAmount.attr('step')).toBe('1')
      expect($purchaseAmount.attr('min')).toBe('0')
    })
  })

  describe('Vertical', () => {
    it('displays vertical label', async () => {
      const response = await server.inject(request)
      const $ = cheerio.load(response.result)
      expect($('label[for="vertical"]').text()).toBe('Goods description')
    })

    it('displays vertical select', async () => {
      const response = await server.inject(request)
      const $ = cheerio.load(response.result)

      expect($('select[name="vertical"]').length).toBe(1)
    })

    it('displays vertical options', async () => {
      const response = await server.inject(request)
      const $ = cheerio.load(response.result)
      const options = $('select[name="vertical"] option')
        .map(function() {
          return $(this).text()
        })
        .get()

      expect(options).toEqual([
        '- Select -',
        'Kitchen',
        'Carpentry',
        'Electrical',
        'Plumbing',
        'Painting',
        'Boiler installation',
        'Masonry',
        'Gardening',
        'Glaziers',
        'Fenestration',
        'Other'
      ])
    })
  })

  describe('Product category', () => {
    it('displays financing product label', async () => {
      const response = await server.inject(request)
      const $ = cheerio.load(response.result)
      expect($('label[for="productCategory"]').text()).toBe('Financing product')
    })

    it('displays product category radio inputs', async () => {
      const response = await server.inject(request)
      const $ = cheerio.load(response.result)
      expect($('input[name="productCategory"][type="radio"]').length).toBe(3)
    })

    it('displays financing product titles', async () => {
      const response = await server.inject(request)
      const $ = cheerio.load(response.result)
      const titles = $('.product-toggle__title')
        .map(function() {
          return $(this).text()
        })
        .get()

      expect(titles).toEqual([
        'Interest free',
        'Buy now, pay later',
        'Interest bearing'
      ])
    })

    it('displays financing product descriptions', async () => {
      const response = await server.inject(request)
      const $ = cheerio.load(response.result)
      const descriptions = $('.product-toggle__description')
        .map(function() {
          return $(this).text()
        })
        .get()

      expect(descriptions).toEqual([
        'Equal payments at 0% interest',
        'Defer the payment. The customer cannot settle during the deferred period. Interest only accrues over the repayment period not the total loan duration',
        'Interest accruing loan where borrower pays back both interest and principal'
      ])
    })

    it('defaults to Interest free product', async () => {
      const response = await server.inject(request)
      const $ = cheerio.load(response.result)

      expect($('input:checked').val()).toEqual('1')
    })

    it('displays Interest free term select', async () => {
      const response = await server.inject(request)
      const $ = cheerio.load(response.result)

      expect($('select[name="maxTerm"]').length).toBe(1)
    })

    it('displays Interest free term options', async () => {
      const response = await server.inject(request)
      const $ = cheerio.load(response.result)
      const options = $('select[name="maxTerm"] option')
        .map(function() {
          return $(this).text()
        })
        .get()

      expect(options).toEqual([
        '- Select -',
        '6 Months',
        '9 Months',
        '10 Months',
        '12 Months',
        '24 Months',
        '36 Months',
        '48 Months',
        '60 Months',
        '84 Months',
        '120 Months'
      ])
    })
  })
})
