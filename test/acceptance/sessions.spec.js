'use strict'

jest.mock('services/auth.service.js')

const cheerio = require('cheerio')
const uuid = require('uuid')

const authService = require('services/auth.service.js')

const {initServer, stopServer, getTemplate} = require('support/server')

describe('/login', () => {
  let server

  beforeAll(async () => {
    server = await initServer()
  })

  afterAll(async () => {
    await stopServer()
  })

  describe('GET /login', () => {
    describe('when request is authenticated', () => {
      const request = {
        method: 'GET',
        url: '/login',
        credentials: {}
      }

      it('redirects to /applications valid credentials', async () => {
        const response = await server.inject(request)
        expect(response.statusCode).toBe(302)
        expect(response.headers.location).toBe('/applications')
      })
    })

    describe('when request is not authenticated', () => {
      const request = {
        method: 'GET',
        url: '/login'
      }

      it('responds with status code 200 "OK"', async () => {
        const response = await server.inject(request)
        expect(response.statusCode).toBe(200)
      })

      it('responds with html content type', async () => {
        const response = await server.inject(request)
        expect(response.headers['content-type']).toMatch(/^text\/html/)
      })

      it('responds with the "sessions/new" template', async () => {
        const response = await server.inject(request)
        expect(getTemplate(response)).toBe('sessions/new')
      })

      it('renders login form', async () => {
        const response = await server.inject(request)
        const $ = cheerio.load(response.result)
        expect($('form').html().length).toBeGreaterThan(0)
      })

      it('login form has correct parameters', async () => {
        const response = await server.inject(request)
        const $ = cheerio.load(response.result)
        expect(
          $('form')
            .attr('method')
            .toLowerCase()
        ).toBe('post')
        expect($('form').attr('action')).toBe('/login')
      })

      it('renders email input', async () => {
        const response = await server.inject(request)
        const $ = cheerio.load(response.result)
        expect($('input[name="email"]').length).toBeGreaterThan(0)
        expect($('input[name="email"]').attr('type')).toBe('email')
        expect($('input[name="email"]').attr('placeholder')).toBe(
          'eg. john@vendigo.com'
        )
      })

      it('renders password input', async () => {
        const response = await server.inject(request)
        const $ = cheerio.load(response.result)
        expect($('input[name="password"]').length).toBeGreaterThan(0)
        expect($('input[name="password"]').attr('type')).toBe('password')
      })

      it('renders submit button', async () => {
        const response = await server.inject(request)
        const $ = cheerio.load(response.result)
        expect($('button[type="submit"]').length).toEqual(1)
      })
    })
  })

  describe('POST /login', () => {
    describe('invalid details', () => {
      const request = {
        method: 'POST',
        url: '/login'
      }

      it('responds with the "sessions/new" template', async () => {
        request.payload = {
          email: 'a@a.com',
          password: 'secret'
        }

        const response = await server.inject(request)
        expect(response.statusCode).toBe(200)
        expect(getTemplate(response)).toBe('sessions/new')
      })

      it('responds with the error message for missing email', async () => {
        request.payload = {
          password: 'secret'
        }
        const response = await server.inject(request)

        const $ = cheerio.load(response.result)
        expect($('.vf-global-errors li').length).toBe(1)
        expect($('.vf-global-errors li').html()).toBe(
          '&quot;email&quot; is required'
        )
      })

      it('responds with the error message for invalid email', async () => {
        request.payload = {
          email: 'notemail',
          password: 'secret'
        }
        const response = await server.inject(request)

        const $ = cheerio.load(response.result)
        expect($('.vf-global-errors li').length).toBe(1)
        expect($('.vf-global-errors li').html()).toBe(
          '&quot;email&quot; must be a valid email'
        )
      })

      it('responds with the error message for missing password', async () => {
        request.payload = {
          email: 'a@a.com'
        }
        const response = await server.inject(request)

        const $ = cheerio.load(response.result)
        expect($('.vf-global-errors li').length).toBe(1)
        expect($('.vf-global-errors li').html()).toBe(
          '&quot;password&quot; is required'
        )
      })

      it('responds with the error message for non-existant user', async () => {
        request.payload = {
          email: 'a@a.com',
          password: 'secret'
        }
        const response = await server.inject(request)

        const $ = cheerio.load(response.result)
        expect($('.vf-global-errors li').length).toBe(1)
        expect($('.vf-global-errors li').html()).toBe(
          'Invalid email or password'
        )
      })
    })

    describe('valid details', () => {
      let user

      beforeEach(() => {
        user = mockUser().user
        authService.__setMockUser(user)
      })

      afterEach(() => {
        authService.__resetMockUsers()
      })

      it('should 302 redirect', async () => {
        const response = await server.inject(getRequest(user))
        expect(response.statusCode).toBe(302)
      })

      it('redirects to /applications', async () => {
        const response = await server.inject(getRequest(user))
        expect(response.headers.location).toBe('/applications')
      })

      function getRequest(user) {
        return {
          method: 'POST',
          url: '/login',
          payload: {
            email: user.email,
            password: 'secret'
          }
        }
      }
    })
  })

  function mockUser() {
    const userUuid = uuid()
    const user = {
      id: userUuid,
      email: 'a@a.test.com',
      passwordHash:
        '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm'
    }
    return {userUuid, user}
  }
})
