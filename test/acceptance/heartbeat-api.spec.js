'use strict'

const {initServer, stopServer} = require('support/server')

describe('GET /heartbeat', () => {
  let server

  const request = {
    method: 'GET',
    url: '/heartbeat'
  }

  beforeAll(async () => {
    server = await initServer()
  })

  afterAll(async () => {
    await stopServer()
  })

  it('responds with status code 200 "OK"', async () => {
    const response = await server.inject(request)
    expect(response.statusCode).toBe(200)
  })

  it('responds with body content "OK"', async () => {
    const response = await server.inject(request)
    expect(response.result).toBe('OK')
  })

  it('responds with plain text content type', async () => {
    const response = await server.inject(request)
    expect(response.headers['content-type']).toMatch(/^text\/plain/)
  })
})
