'use strict'

const cheerio = require('cheerio')

const {initServer, stopServer, getTemplate} = require('support/server')

describe('GET /applications/{{uuid}}', () => {
  let server

  const request = {
    method: 'get',
    url: '/applications/abc123',
    credentials: {}
  }

  beforeAll(async () => {
    server = await initServer()
  })

  afterAll(async () => {
    await stopServer()
  })

  it('responds with 200 "OK" and html content type', async () => {
    const response = await server.inject(request)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toMatch(/^text\/html/)
  })

  it('responds with the "applications/show" template', async () => {
    const response = await server.inject(request)
    expect(getTemplate(response)).toBe('applications/show')
  })

  it('renders expected header', async () => {
    const response = await server.inject(request)
    const $ = cheerio.load(response.result)
    expect($('h1').text()).toBe('Application')
  })
})
