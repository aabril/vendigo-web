'use strict'

const productFieldsHelper = require('views/helpers/product-fields')

describe('product-fields.helper', () => {
  it('should kebab case the name of a product and add fields', () => {
    const productCategory = {id: 1, name: 'Some Fancy Product Vertical name'}
    expect(productFieldsHelper(productCategory)).toBe(
      'some-fancy-product-vertical-name-fields'
    )
  })
})
