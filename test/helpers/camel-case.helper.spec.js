'use strict'

const camelCaseHelper = require('views/helpers/camel-case')

describe('camel-case.helper', () => {
  it('kebab-case to camelCase', () => {
    expect(camelCaseHelper('kebab-case')).toBe('kebabCase')
  })

  it('snake_case to camelCase', () => {
    expect(camelCaseHelper('snake_case')).toBe('snakeCase')
  })

  it('space case to camelCase', () => {
    expect(camelCaseHelper('space case')).toBe('spaceCase')
  })
})
