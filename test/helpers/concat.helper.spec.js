'use strict'

const concatHelper = require('views/helpers/concat')

describe('concat.helper', () => {
  it('concatenates strings', () => {
    expect(concatHelper('join', 'me')).toBe('joinme')
  })

  it('concatenates multiple strings', () => {
    expect(concatHelper('join', 'me', 'up')).toBe('joinmeup')
  })

  it('concatenates strings with custom separator', () => {
    const options = {
      hash: {separator: '-'}
    }
    expect(concatHelper('join', 'me', options)).toBe('join-me')
  })

  it('concatenates strings with custom separator regardless of argument order', () => {
    const options = {
      hash: {separator: '.'}
    }
    expect(concatHelper('join', options, 'me', 'up')).toBe('join.me.up')
  })
})
