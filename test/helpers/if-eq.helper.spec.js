'use strict'

const ifEqHelper = require('views/helpers/if-eq')

describe('if-eq.helper', () => {
  it('should be true if values match', () => {
    const options = {fn: jest.fn(), inverse: jest.fn()}

    ifEqHelper('a', 'a', options)

    expect(options.fn).toHaveBeenCalled()
    expect(options.inverse).not.toHaveBeenCalled()
  })

  it('should be false if values do not match', () => {
    const options = {fn: jest.fn(), inverse: jest.fn()}

    ifEqHelper('a', 'b', options)

    expect(options.inverse).toHaveBeenCalled()
    expect(options.fn).not.toHaveBeenCalled()
  })
})
