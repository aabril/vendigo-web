'use strict'

const vfInputHelper = require('views/helpers/vf-input')

describe('vf-input.helper', () => {
  it('should throw an error if n name attribute is supplied', () => {
    expect(() => vfInputHelper({hash: {}})).toThrow(
      '{{vf-input}}: a name attribute must be supplied'
    )
  })

  describe('given no attributes are supplied', () => {
    it('should return an input', () => {
      expect(vfInputHelper({hash: {name: 'a'}})).toHaveHtml(`
          <div class="vf-input"  data-controller="input" data-target="input.wrap">
            <input class="vf-input__input"
                   name="a"
                   data-target="form.a input.input"
                   data-action="focus->input#focus blur->input#blur" />
          </div>
        `)
    })
  })

  describe('given attributes are supplied', () => {
    describe('class', () => {
      it('should apply class name to parent element', () => {
        const inputWithClassName = vfInputHelper({
          hash: {
            name: 'a',
            class: 'some-class-name'
          }
        })

        expect(inputWithClassName).toHaveHtml(`
          <div class="vf-input some-class-name"  data-controller="input" data-target="input.wrap">
            <input class="vf-input__input"
                   name="a"
                   data-target="form.a input.input"
                   data-action="focus->input#focus blur->input#blur" />
          </div>
        `)
      })

      it('should apply multiple class names to parent element', () => {
        const inputWithMultipleClassNames = vfInputHelper({
          hash: {
            name: 'a',
            class: 'some-class-name a-n-other-class'
          }
        })

        expect(inputWithMultipleClassNames).toHaveHtml(`
          <div class="vf-input some-class-name a-n-other-class"  data-controller="input" data-target="input.wrap">
            <input class="vf-input__input"
                   name="a"
                   data-target="form.a input.input"
                   data-action="focus->input#focus blur->input#blur" />
          </div>
        `)
      })
    })

    describe('disabled', () => {
      it('should apply disabled attribute to parent and input', () => {
        const inputWithDisabled = vfInputHelper({
          hash: {
            name: 'a',
            disabled: true
          }
        })

        expect(inputWithDisabled).toHaveHtml(`
          <div class="vf-input"  data-controller="input" data-target="input.wrap" disabled>
            <input class="vf-input__input"
                   name="a"
                   disabled
                   data-target="form.a input.input"
                   data-action="focus->input#focus blur->input#blur" />
          </div>
        `)
      })
    })

    describe('readonly', () => {
      it('should apply readonly attribute to parent and input', () => {
        const inputWithReadonly = vfInputHelper({
          hash: {
            name: 'a',
            readonly: true
          }
        })

        expect(inputWithReadonly).toHaveHtml(`
          <div class="vf-input"  data-controller="input" data-target="input.wrap" readonly>
            <input class="vf-input__input"
                   name="a"
                   readonly
                   data-target="form.a input.input"
                   data-action="focus->input#focus blur->input#blur" />
          </div>
        `)
      })
    })

    describe('data-controller', () => {
      it('should add data-controller to existing data-controller attribute on parent element', () => {
        const inputWithController = vfInputHelper({
          hash: {
            name: 'a',
            controller: 'some-controller'
          }
        })

        expect(inputWithController).toHaveHtml(`
          <div class="vf-input"  data-controller="input some-controller" data-target="input.wrap">
            <input class="vf-input__input"
                   name="a"
                   data-target="form.a input.input"
                   data-action="focus->input#focus blur->input#blur" />
          </div>
        `)
      })

      it('should apply multiple class names to parent element', () => {
        const inputWithMultipleControllers = vfInputHelper({
          hash: {
            name: 'a',
            controller: 'some-controller a-n-other-controller'
          }
        })

        expect(inputWithMultipleControllers).toHaveHtml(`
          <div class="vf-input"  data-controller="input some-controller a-n-other-controller" data-target="input.wrap">
            <input class="vf-input__input"
                   name="a"
                   data-target="form.a input.input"
                   data-action="focus->input#focus blur->input#blur" />
          </div>
        `)
      })
    })

    describe('data-action', () => {
      it('should add data-action to existing data-action attribute on input element', () => {
        const inputWithAction = vfInputHelper({
          hash: {
            name: 'a',
            action: 'some-action'
          }
        })

        expect(inputWithAction).toHaveHtml(`
          <div class="vf-input"  data-controller="input" data-target="input.wrap">
            <input class="vf-input__input"
                   name="a"
                   data-target="form.a input.input"
                   data-action="focus->input#focus blur->input#blur some-action" />
          </div>
        `)
      })

      it('should apply multiple actions to input element', () => {
        const inputWithMultipleActions = vfInputHelper({
          hash: {
            name: 'a',
            action: 'some-action a-n-other-action'
          }
        })

        expect(inputWithMultipleActions).toHaveHtml(`
          <div class="vf-input"  data-controller="input" data-target="input.wrap">
            <input class="vf-input__input"
                   name="a"
                   data-target="form.a input.input"
                   data-action="focus->input#focus blur->input#blur some-action a-n-other-action" />
          </div>
        `)
      })
    })

    describe('data-target', () => {
      it('should add data-target to existing data-target attribute on input element', () => {
        const inputWithTarget = vfInputHelper({
          hash: {
            name: 'a',
            target: 'some-target'
          }
        })

        expect(inputWithTarget).toHaveHtml(`
          <div class="vf-input"  data-controller="input" data-target="input.wrap">
            <input class="vf-input__input"
                   name="a"
                   data-target="form.a input.input some-target"
                   data-action="focus->input#focus blur->input#blur" />
          </div>
        `)
      })

      it('should apply multiple targets to input element', () => {
        const inputWithMultipleTargets = vfInputHelper({
          hash: {
            name: 'a',
            target: 'some-target a-n-other-target'
          }
        })

        expect(inputWithMultipleTargets).toHaveHtml(`
          <div class="vf-input"  data-controller="input" data-target="input.wrap">
            <input class="vf-input__input"
                   name="a"
                   data-target="form.a input.input some-target a-n-other-target"
                   data-action="focus->input#focus blur->input#blur" />
          </div>
        `)
      })
    })
  })

  describe('prepend', () => {
    it('should add prepend value before input', () => {
      const inputWithPrependAttr = vfInputHelper({
        hash: {
          name: 'a',
          prepend: '£'
        }
      })

      expect(inputWithPrependAttr).toHaveHtml(`
          <div class="vf-input"  data-controller="input" data-target="input.wrap">
            <div class="vf-input__prepend"><span>£</span></div>
            <input class="vf-input__input"
                   name="a"
                   data-target="form.a input.input"
                   data-action="focus->input#focus blur->input#blur" />
          </div>
        `)
    })
  })

  describe('append', () => {
    it('should add append value after input', () => {
      const inputWithPrependAttr = vfInputHelper({
        hash: {
          name: 'a',
          append: '.00'
        }
      })

      expect(inputWithPrependAttr).toHaveHtml(`
          <div class="vf-input"  data-controller="input" data-target="input.wrap">
            <input class="vf-input__input"
                   name="a"
                   data-target="form.a input.input"
                   data-action="focus->input#focus blur->input#blur" />
             <div class="vf-input__append"><span>.00</span></div>
          </div>
        `)
    })
  })
})
