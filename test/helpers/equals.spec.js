'use strict'

const equalsHelper = require('views/helpers/equals')

describe('equals.helper', () => {
  describe('comparing string', () => {
    it('should return true when string match', () => {
      expect(equalsHelper('abc', 'abc')).toBeTruthy()
    })

    it('should return false when strings do not match', () => {
      expect(equalsHelper('abc', 'def')).toBeFalsy()
    })
  })
})
