'use strict'

const ifIncludesHelper = require('views/helpers/if-includes')

describe('if-includes.helper', () => {
  describe('string includes', () => {
    it('should be true if string contains value', () => {
      const options = {fn: jest.fn(), inverse: jest.fn(), hash: {}}

      ifIncludesHelper('abc', 'a', options)

      expect(options.fn).toHaveBeenCalled()
      expect(options.inverse).not.toHaveBeenCalled()
    })

    it('should be false if string does not include value', () => {
      const options = {fn: jest.fn(), inverse: jest.fn(), hash: {}}

      ifIncludesHelper('abc', 'd', options)

      expect(options.inverse).toHaveBeenCalled()
      expect(options.fn).not.toHaveBeenCalled()
    })

    describe('with fromIndex', () => {
      it('should be true if string contains value starting at specified index', () => {
        const options = {
          fn: jest.fn(),
          inverse: jest.fn(),
          hash: {fromIndex: 1}
        }

        ifIncludesHelper('xabc', 'a', options)

        expect(options.fn).toHaveBeenCalled()
        expect(options.inverse).not.toHaveBeenCalled()
      })

      it('should be false if string does not include value starting at specified index', () => {
        const options = {
          fn: jest.fn(),
          inverse: jest.fn(),
          hash: {fromIndex: 1}
        }

        ifIncludesHelper('dxxx', 'd', options)

        expect(options.inverse).toHaveBeenCalled()
        expect(options.fn).not.toHaveBeenCalled()
      })
    })
  })

  describe('array includes', () => {
    it('should be true if array contains value', () => {
      const options = {fn: jest.fn(), inverse: jest.fn(), hash: {}}

      ifIncludesHelper(['a', 'b', 'c'], 'a', options)

      expect(options.fn).toHaveBeenCalled()
      expect(options.inverse).not.toHaveBeenCalled()
    })

    it('should be false if array does not include value', () => {
      const options = {fn: jest.fn(), inverse: jest.fn(), hash: {}}

      ifIncludesHelper(['a', 'b', 'c'], 'd', options)

      expect(options.inverse).toHaveBeenCalled()
      expect(options.fn).not.toHaveBeenCalled()
    })

    describe('with fromIndex', () => {
      it('should be true if array contains value starting at specified index', () => {
        const options = {
          fn: jest.fn(),
          inverse: jest.fn(),
          hash: {fromIndex: 1}
        }

        ifIncludesHelper(['x', 'a', 'b', 'c'], 'a', options)

        expect(options.fn).toHaveBeenCalled()
        expect(options.inverse).not.toHaveBeenCalled()
      })

      it('should be false if array does not include value starting at specified index', () => {
        const options = {
          fn: jest.fn(),
          inverse: jest.fn(),
          hash: {fromIndex: 1}
        }

        ifIncludesHelper(['d', 'x'], 'd', options)

        expect(options.inverse).toHaveBeenCalled()
        expect(options.fn).not.toHaveBeenCalled()
      })
    })
  })

  describe('object includes', () => {
    it('should be true if object contains value', () => {
      const options = {fn: jest.fn(), inverse: jest.fn(), hash: {}}

      ifIncludesHelper({a: 'a', b: 'b', c: 'c'}, 'a', options)

      expect(options.fn).toHaveBeenCalled()
      expect(options.inverse).not.toHaveBeenCalled()
    })

    it('should be false if object does not include value', () => {
      const options = {fn: jest.fn(), inverse: jest.fn(), hash: {}}

      ifIncludesHelper({a: 'a', b: 'b', c: 'c'}, 'd', options)

      expect(options.inverse).toHaveBeenCalled()
      expect(options.fn).not.toHaveBeenCalled()
    })

    describe('with fromIndex', () => {
      it('should be true if object contains value starting at specified index', () => {
        const options = {
          fn: jest.fn(),
          inverse: jest.fn(),
          hash: {fromIndex: 1}
        }

        ifIncludesHelper({x: 'x', a: 'a', b: 'b', c: 'c'}, 'a', options)

        expect(options.fn).toHaveBeenCalled()
        expect(options.inverse).not.toHaveBeenCalled()
      })

      it('should be false if object does not include value starting at specified index', () => {
        const options = {
          fn: jest.fn(),
          inverse: jest.fn(),
          hash: {fromIndex: 1}
        }

        ifIncludesHelper({d: 'd', x: 'x'}, 'd', options)

        expect(options.inverse).toHaveBeenCalled()
        expect(options.fn).not.toHaveBeenCalled()
      })
    })
  })
})
