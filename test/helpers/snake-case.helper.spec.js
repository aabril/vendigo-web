'use strict'

const snakeCaseHelper = require('views/helpers/snake-case')

describe('snake-case.helper', () => {
  it('kebab-case to snake_case', () => {
    expect(snakeCaseHelper('kebab-case')).toBe('kebab_case')
  })

  it('camelCase to snake_case', () => {
    expect(snakeCaseHelper('camelCase')).toBe('camel_case')
  })

  it('space case to snake_case', () => {
    expect(snakeCaseHelper('space case')).toBe('space_case')
  })
})
