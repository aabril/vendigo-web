'use strict'

const kebabCaseHelper = require('views/helpers/kebab-case')

describe('kebab-case.helper', () => {
  it('camelCase to kebab-case', () => {
    expect(kebabCaseHelper('camelCase')).toBe('camel-case')
  })

  it('snake_case to kebab-case', () => {
    expect(kebabCaseHelper('snake_case')).toBe('snake-case')
  })

  it('space case to kebab-case', () => {
    expect(kebabCaseHelper('space case')).toBe('space-case')
  })
})
