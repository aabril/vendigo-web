'use strict'

const i18nHelper = require('views/helpers/i18n')

describe('i18n.helper', () => {
  describe('with language argument', () => {
    let helper
    const language = {
      a: 'first',
      b: {c: 'nested'}
    }

    beforeEach(() => {
      helper = new MockHandlebars(i18nHelper).helper
    })

    it('matches key in language', () => {
      const options = {hash: {language}}
      expect(helper('a', options)).toBe('first')
    })

    it('matches nested key in language', () => {
      const options = {hash: {language}}
      expect(helper('b.c', options)).toBe('nested')
    })

    it('throws an error if key is not a string', () => {
      const options = {hash: {language}}
      expect(() => helper({not: 'a string'}, options)).toThrow(
        '{{i18n}} helper: invalid key. Object keys must be formatted as strings.'
      )
    })

    it('throws an error if language is undefined', () => {
      const options = {hash: {language: undefined}}
      expect(() => helper('a', options)).toThrow(
        '{{i18n}} helper: the "language" parameter is not defined.'
      )
    })

    it('throws an error if key is unknown', () => {
      const options = {hash: {language}}
      expect(() => helper('a.unknown', options)).toThrow(
        '{{i18n}} helper: property "a.unknown" is not defined for language.'
      )
    })
  })

  describe('without language argument', () => {
    it('matches key in language', () => {
      const language = {a: 'first'}
      const {helper} = new MockHandlebars(i18nHelper, language)

      expect(helper('a')).toBe('first')
    })

    it('matches nested key in language', () => {
      const language = {b: {c: 'nested'}}
      const {helper} = new MockHandlebars(i18nHelper, language)

      expect(helper('b.c')).toBe('nested')
    })

    it('throws an error if key is not a string', () => {
      const language = {b: {c: 'nested'}}
      const {helper} = new MockHandlebars(i18nHelper, language)

      expect(() => helper({not: 'a string'})).toThrow(
        '{{i18n}} helper: invalid key. Object keys must be formatted as strings.'
      )
    })

    it('throws an error if language is undefined', () => {
      const language = undefined
      const {helper} = new MockHandlebars(i18nHelper, language)

      expect(() => helper('a')).toThrow(
        '{{i18n}} helper: the "language" parameter is not defined.'
      )
    })

    it('throws an error if key is unknown', () => {
      const language = {b: {c: 'nested'}}
      const {helper} = new MockHandlebars(i18nHelper, language)

      expect(() => helper('a.unknown')).toThrow(
        '{{i18n}} helper: property "a.unknown" is not defined for language.'
      )
    })
  })

  function MockHandlebars(helper, language) {
    this.language = language
    this.helper = helper.bind(this)
    return this
  }
})
