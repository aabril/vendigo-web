'use strict'

import joi from 'joi'

import commonSchema from 'schemas/common.schema'

describe('common.schema', () => {
  describe('::isoDate()', () => {
    let isoDateSchema

    beforeEach(() => {
      isoDateSchema = commonSchema.isoDate()
    })

    it('accepts valid ISO 8601 date string', () => {
      const {error} = joi.validate('2018-01-22T08:50:39Z', isoDateSchema)
      expect(error).toBeNull()
    })

    it('trims whitespace', () => {
      const {error, value} = joi.validate(
        '  2018-01-22T08:50:39Z  ',
        isoDateSchema
      )
      expect(error).toBeNull()
      expect(value).toBe('2018-01-22T08:50:39.000Z')
    })

    it('rejects non-string value', () => {
      const {error} = joi.validate(42, isoDateSchema)
      expect(error).not.toBeNull()
      expect(error.message).toBe('"value" must be a string')
    })

    it('rejects invalid ISO 8601 date string', () => {
      const {error} = joi.validate('22-01-2018', isoDateSchema)
      expect(error).not.toBeNull()
      expect(error.message).toBe('"value" must be a valid ISO 8601 date')
    })
  })

  describe('::strMax()', () => {
    let strMaxSchema

    beforeEach(() => {
      strMaxSchema = commonSchema.strMax(3)
    })

    it('accepts valid string', () => {
      const {error} = joi.validate('abc', strMaxSchema)
      expect(error).toBeNull()
    })

    it('trims whitespace', () => {
      const {error, value} = joi.validate('  abc  ', strMaxSchema)
      expect(error).toBeNull()
      expect(value).toBe('abc')
    })

    it('rejects non-string value', () => {
      const {error} = joi.validate(42, strMaxSchema)
      expect(error).not.toBeNull()
      expect(error.message).toBe('"value" must be a string')
    })

    it('rejects string over max value of characters', () => {
      const {error} = joi.validate('abcd', strMaxSchema)
      expect(error).not.toBeNull()
      expect(error.message).toMatch(/be less than or equal to 3/)
    })
  })

  describe('::uuid()', () => {
    let uuidSchema

    beforeEach(() => {
      uuidSchema = commonSchema.uuid()
    })

    it('accepts valid UUID value', () => {
      const {error} = joi.validate(
        'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
        uuidSchema
      )
      expect(error).toBeNull()
    })

    it('rejects non-string value', () => {
      const {error} = joi.validate(42, uuidSchema)
      expect(error).not.toBeNull()
      expect(error.message).toBe('"value" must be a string')
    })

    it('rejects non-hexadecimal value', () => {
      const {error} = joi.validate('foobar', uuidSchema)
      expect(error).not.toBeNull()
      expect(error.message).toBe('"value" must be a valid GUID')
    })
  })
})
