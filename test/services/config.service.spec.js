'use strict'

// sut
const configService = require('services/config.service')

describe('config.service', () => {
  describe('::get()', () => {
    it('returns requested environment variable', () => {
      expect(configService.get('NODE_ENV')).toBe('test')
    })

    it('accepts colon style variable names (like nconf)', () => {
      process.env.FOO_BAR_BAZ = 'value'
      expect(configService.get('NODE:ENV')).toBe('test')
      expect(configService.get('FOO:BAR:BAZ')).toBe('value')
      delete process.env.FOO_BAR_BAZ
    })

    it('returns undefined for unknown variable names', () => {
      expect(configService.get('MR:MAGOO')).toBe(undefined)
    })

    it('returns undefined for invalid variable names', () => {
      expect(configService.get(47)).toBe(undefined)
    })
  })
})
