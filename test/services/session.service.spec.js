'use strict'

const sessionService = require('services/session.service')
const given = describe

describe('session.service', () => {
  describe('::userFromState(state)', () => {
    given('state.sid exists', () => {
      given('user role is admin', () => {
        it('creates user with isAdmin true', () => {
          const state = {
            sid: {
              email: 'some@email.com',
              role: 'admin'
            }
          }

          expect(sessionService.userFromState(state)).toEqual({
            email: 'some@email.com',
            isAdmin: true,
            isCustomer: false,
            isMerchant: false
          })
        })
      })

      given('user role is customer', () => {
        it('creates user with isCustomer true', () => {
          const state = {
            sid: {
              email: 'some@email.com',
              role: 'customer'
            }
          }

          expect(sessionService.userFromState(state)).toEqual({
            email: 'some@email.com',
            isAdmin: false,
            isCustomer: true,
            isMerchant: false
          })
        })
      })

      given('user role is merchant', () => {
        it('creates user with isMerchant true', () => {
          const state = {
            sid: {
              email: 'some@email.com',
              role: 'merchant'
            }
          }

          expect(sessionService.userFromState(state)).toEqual({
            email: 'some@email.com',
            isAdmin: false,
            isCustomer: false,
            isMerchant: true
          })
        })
      })
    })

    given('state.sid is undefined', () => {
      it('returns undefined', () => {
        const state = {sid: undefined}

        expect(sessionService.userFromState(state)).toEqual(undefined)
      })
    })

    given('state.sid is null', () => {
      it('returns undefined', () => {
        const state = {sid: null}

        expect(sessionService.userFromState(state)).toEqual(undefined)
      })
    })

    given('state is undefined', () => {
      it('returns undefined', () => {
        const state = undefined

        expect(sessionService.userFromState(state)).toEqual(undefined)
      })
    })

    given('state is null', () => {
      it('returns undefined', () => {
        const state = null

        expect(sessionService.userFromState(state)).toEqual(undefined)
      })
    })
  })
})
