/* eslint no-console: "off" */
'use strict'

const loggerService = require('services/logger.service')

const mock = require('support/helpers/mock.helper')

describe('logger.service', () => {
  describe('::debug()', () => {
    beforeEach(() => {
      mock(console, 'dir')
    })

    afterEach(() => {
      console.dir.restore()
    })

    it('calls console.dir', () => {
      loggerService.debug()
      expect(console.dir).toHaveBeenCalled()
    })

    it('calls console dir with provided argument', () => {
      loggerService.debug({foo: 'bar'})
      expect(console.dir.mock.calls[0][0]).toEqual({foo: 'bar'})
    })

    it('calls console dir with expected options', () => {
      loggerService.debug({foo: 'bar'})
      expect(console.dir.mock.calls[0][1]).toEqual({
        showHidden: true,
        depth: 2,
        colors: true
      })
    })
  })

  describe('::error()', () => {
    beforeEach(() => {
      mock(console, 'error')
    })

    afterEach(() => {
      console.error.restore()
    })

    it('calls console.error', () => {
      loggerService.error(['db'], 'whatever')
      expect(console.error).toHaveBeenCalledWith(
        "\u001b[31m✖\u001b[39m [ 'db', 'error' ] whatever"
      )
    })
  })

  describe('::info()', () => {
    beforeEach(() => {
      mock(console, 'info')
    })

    afterEach(() => {
      console.info.restore()
    })

    it('calls console info', () => {
      loggerService.info('whatever')
      expect(console.info).toHaveBeenCalledWith(
        "\u001b[34mℹ\u001b[39m [ 'info' ] whatever"
      )
    })
  })

  describe('::warn()', () => {
    beforeEach(() => {
      mock(console, 'warn')
    })

    afterEach(() => {
      console.warn.restore()
    })

    it('calls console.warn', () => {
      loggerService.warn('whatever')
      expect(console.warn).toHaveBeenCalledWith(
        "\u001b[33m⚠\u001b[39m [ 'warn' ] whatever"
      )
    })
  })
})
