'use strict'

const envService = require('services/env.service')

const given = describe

describe('env.service', () => {
  describe('::isDevelopment()', () => {
    given('development environment', () => {
      it('returns true', () => {
        const restore = setNodeEnvTo('development')
        expect(envService.isDevelopment()).toBe(true)
        restore()
      })
    })

    given('production-like environment', () => {
      it('returns false', () => {
        const restore = setNodeEnvTo('staging')
        expect(envService.isDevelopment()).toBe(false)
        restore()
      })
    })

    given('invalid environment', () => {
      it('returns false', () => {
        const restore = setNodeEnvTo('whatever')
        expect(envService.isDevelopment()).toBe(false)
        restore()
      })
    })
  })

  describe('::isProduction()', () => {
    given('production environment', () => {
      it('returns true', () => {
        const restore = setNodeEnvTo('production')
        expect(envService.isProduction()).toBe(true)
        restore()
      })
    })

    given('non-production environment', () => {
      it('returns false', () => {
        const restore = setNodeEnvTo('staging')
        expect(envService.isProduction()).toBe(false)
        restore()
      })
    })

    given('invalid environment', () => {
      it('returns false', () => {
        const restore = setNodeEnvTo('whatever')
        expect(envService.isProduction()).toBe(false)
        restore()
      })
    })
  })

  describe('::isQa()', () => {
    given('qa environment', () => {
      it('returns true', () => {
        const restore = setNodeEnvTo('qa')
        expect(envService.isQa()).toBe(true)
        restore()
      })
    })

    given('non-qa environment', () => {
      it('returns false', () => {
        const restore = setNodeEnvTo('production')
        expect(envService.isQa()).toBe(false)
        restore()
      })
    })

    given('invalid environment', () => {
      it('returns false', () => {
        const restore = setNodeEnvTo('whatever')
        expect(envService.isQa()).toBe(false)
        restore()
      })
    })
  })

  describe('::isTest()', () => {
    given('test environment', () => {
      it('returns true', () => {
        const restore = setNodeEnvTo('test')
        expect(envService.isTest()).toBe(true)
        restore()
      })
    })

    given('non-test environment', () => {
      it('returns false', () => {
        const restore = setNodeEnvTo('production')
        expect(envService.isTest()).toBe(false)
        restore()
      })
    })

    given('invalid environment', () => {
      it('returns false', () => {
        const restore = setNodeEnvTo('whatever')
        expect(envService.isTest()).toBe(false)
        restore()
      })
    })
  })
})

function setNodeEnvTo(env) {
  const realEnv = process.env['NODE_ENV']
  process.env['NODE_ENV'] = env
  return function restoreEnv() {
    process.env['NODE_ENV'] = realEnv
  }
}
