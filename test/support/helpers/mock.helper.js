'use strict'

module.exports = function mock(object, methodName) {
  object[`_${methodName}`] = object[methodName]
  object[methodName] = jest.fn()
  object[methodName].restore = function() {
    object[methodName] = object[`_${methodName}`]
    delete object[`_${methodName}`]
  }
  object[methodName].resolves = function(results) {
    object[methodName].mockImplementation(() => {
      return Promise.resolve(results)
    })
  }
  object[methodName].rejects = function(results) {
    object[methodName].mockImplementation(() => {
      return Promise.reject(results)
    })
  }
  return object[methodName]
}
