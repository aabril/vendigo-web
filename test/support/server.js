'use strict'

const {initServer} = require('../../server')
const {disconnectDbConnection} = require('services/db.service')

module.exports = {
  async initServer() {
    const server = await initServer()
    return server
  },
  async stopServer() {
    disconnectDbConnection()
  },
  getTemplate(res) {
    return res.request.response.source.template
  }
}
