import {HtmlDiffer} from 'html-differ'
import logger from 'html-differ/lib/logger'

const diff = new HtmlDiffer({})

export default {
  toHaveHtml(actual, expected) {
    const pass = diff.isEqual(actual, expected)

    if (pass) {
      return {
        message: () => `expected ${actual} not to have html ${expected}`,
        pass
      }
    } else {
      return {
        message: () => {
          const diffText = logger.getDiffText(diff.diffHtml(actual, expected))
          return `
            expected ${actual} to have html ${expected}.
            
            ${diffText}

          `
        },
        pass
      }
    }
  }
}
