import DOMAssertions from 'qunit-dom/lib/assertions'

QUnit.extend(QUnit.assert, {
  dom(target, rootElement) {
    const rootEl = rootElement || this.dom.rootElement
    return new DOMAssertions(target, rootEl, this)
  }
})
