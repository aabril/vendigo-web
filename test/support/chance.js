'use strict'

// 3rd-party
const Chance = require('chance')

// init
const chance = new Chance()
const mixin = {}

mixin.address = function(attrs) {
  return Object.assign(
    {
      building: chance.string(),
      street: chance.street(),
      city: chance.city(),
      postCode: chance.postal(),
      country: chance.country()
    },
    attrs || {}
  )
}

mixin.application = function(attrs) {
  return Object.assign(
    {
      purchaseAmount: chance.floating({min: 5000, max: 25000, fixed: 2}),
      depositAmount: chance.floating({min: 100, max: 2000, fixed: 2}),
      verticalId: 1,
      productId: 1
    },
    attrs || {}
  )
}

mixin.bank = function(attrs) {
  return Object.assign(
    {
      name: chance.string(),
      accountNumber: chance.natural({min: 1111111111, max: 9999999999}),
      sortCode: chance.natural({min: 1111, max: 9999})
    },
    attrs || {}
  )
}

chance.mixin(mixin)

module.exports = chance
