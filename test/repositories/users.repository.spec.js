'use strict'

// sut
const usersRepository = require('repositories/users.repository')

describe('users.repository', () => {
  describe('::getByEmail()', () => {
    it('is implemented', () => {
      expect(usersRepository.getByEmail).toBeInstanceOf(Function)
    })
  })
})
