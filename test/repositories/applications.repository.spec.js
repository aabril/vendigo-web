'use strict'

// sut
const applicationsRepository = require('repositories/applications.repository')

describe('applications.repository', () => {
  describe('::all()', () => {
    it('is implemented', () => {
      expect(applicationsRepository.all).toBeInstanceOf(Function)
    })
  })

  describe('::get()', () => {
    it('is implemented', () => {
      expect(applicationsRepository.get).toBeInstanceOf(Function)
    })
  })

  describe('::create()', () => {
    it('is implemented', () => {
      expect(applicationsRepository.create).toBeInstanceOf(Function)
    })
  })

  describe('::update()', () => {
    it('is implemented', () => {
      expect(applicationsRepository.update).toBeInstanceOf(Function)
    })
  })
})
