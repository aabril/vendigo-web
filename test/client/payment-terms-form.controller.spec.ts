import {ApplicationTestCase} from '@stimulus/test'
import '../support/qunit.assertions'

import {PaymentTermsFormController} from '../../lib/assets/components/payment-terms-form'

export default class PaymentTermsFormTests extends ApplicationTestCase {

  fixtureHTML = `
    <form data-controller="payment-terms-form">
      <input type="text" data-target="payment-terms-form.purchaseAmount" />
      <select data-target="payment-terms-form.depositPercentage"
              data-action="change->payment-terms-form#updateDeposit" >
        <option disabled selected>None</option>
        <option value="10"></option>
        <option value="15"></option>
      </select>
      <input type="text" data-target="payment-terms-form.deposit">
    </form>
    `

  setupApplication() {
    this.application.register('payment-terms-form', PaymentTermsFormController)
  }

  'test PaymentTermsForm#updateDeposit should update depsoit field with percentage of purchase amount'() {
    this.purchaseAmountTarget.value = '100'

    this.setValue(this.depositPercentage, '10')

    this.assert.dom(this.depositTarget).hasValue('10')
  }

  'test PaymentTermsForm#updateDeposit should round down deposit amount when deciaml is below .5'() {
    this.purchaseAmountTarget.value = '135'

    this.setValue(this.depositPercentage, '15')

    this.assert.dom(this.depositTarget).hasValue('20')
  }

  'test PaymentTermsForm#updateDeposit should round up deposit amount when deciaml is above .5'() {
    this.purchaseAmountTarget.value = '137'

    this.setValue(this.depositPercentage, '15')

    this.assert.dom(this.depositTarget).hasValue('21')
  }

  setValue(el, val) {
    el.value = val
    this.triggerEvent(el, 'change')
  }

  get depositPercentage() {
    return this.findElement('select') as HTMLSelectElement
  }

  get purchaseAmountTarget() {
    return this.findElement('[data-target$=".purchaseAmount"]') as HTMLInputElement
  }

  get depositTarget() {
    return this.findElement('[data-target$=".deposit"]') as HTMLInputElement
  }
}
