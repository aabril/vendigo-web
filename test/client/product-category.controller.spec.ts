import {ApplicationTestCase} from '@stimulus/test'
import * as sinon from 'sinon'
import '../support/qunit.assertions'

import {ProductCategoryController} from '../../lib/assets/js/controllers/product-category.controller'
import * as productCategoryTemplateService from '../../lib/assets/js/services/product-category-template.service'

export default class ProductCategoryTests extends ApplicationTestCase {

  fixtureHTML = `
    <div data-controller="product-category">
      <input id="firstCategory" type="radio" name="test" data-action="change->product-category#change" checked="checked">
      <input id="secondCategory" type="radio" name="test" data-action="change->product-category#change">
      <div data-target="product-category.fields"></div>
    </div>
    `

  setupApplication() {
    this.application.register('product-category', ProductCategoryController)
  }

  'test ProductCategory#change should request new template'() {
    const done = this.assert.async()
    const expectedTemplate = 'some template'

    const stub = sinon.stub(productCategoryTemplateService, 'fetchTemplate')
    stub
      .withArgs('second-category')
      .resolves(expectedTemplate)

    this.unCheckedInput.click()

    setTimeout(() => {
      this.assert.dom(this.fieldsTarget).hasText(expectedTemplate)
      stub.restore()
      done()
    })
  }

  get unCheckedInput() {
    return this.findElement('input:not(:checked)') as HTMLInputElement
  }

  get fieldsTarget() {
    return this.findElement('[data-target]') as HTMLElement
  }
}
