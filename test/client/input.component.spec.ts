import {ApplicationTestCase} from '@stimulus/test'
import '../support/qunit.assertions'

import {InputController} from '../../lib/assets/components/vf-input'

class InputElement {
  testCase: ApplicationTestCase
  baseSelector: string

  constructor(testCase, name) {
    this.testCase = testCase
    this.baseSelector = `[name=${name}]`
  }

  setInputValue(val) {
    this.input.setAttribute('value', val)
  }

  triggerEvent(eventName) {
    this.testCase.triggerEvent(this.input, eventName)
  }

  get element() {
    return this.testCase.findElement(`div${this.baseSelector}`) as HTMLElement
  }

  private get input() {
    return this.testCase.findElement(`${this.baseSelector} input`) as HTMLInputElement
  }
}

export default class InputTests extends ApplicationTestCase {

  fixtureHTML = `
    <div name="noValidation" data-controller="input">
      <input type="text" data-target="input.input" data-action="focus->input#focus blur->input#blur" />
    </div>
    <div name="withValidation" data-controller="input">
      <input required  type="text" data-target="input.input" data-action="focus->input#focus blur->input#blur" />
    </div>
    `

  setupApplication() {
    this.application.register('input', InputController)
  }

  'test Input#connect'() {
    this.assert.dom(this.inputNoValidation.element).hasClass('is-pristine')
  }

  'test Input#focus should apply is-focused class name'() {
    this.inputNoValidation.triggerEvent('focus')

    this.assert.dom(this.inputNoValidation.element).hasClass('is-focused')
  }

  'test Input#blur should remove is-focused class name'() {
    this.inputNoValidation.triggerEvent('focus')
    this.inputNoValidation.triggerEvent('blur')

    this.assert.dom(this.inputNoValidation.element).lacksClass('is-focused')
  }

  'test Input#blur should apply is-touched class name'() {
    this.inputNoValidation.triggerEvent('blur')

    this.assert.dom(this.inputNoValidation.element).hasClass('is-touched')
  }

  'test Input#blur should apply is-dirty class name when a value has been entered'() {
    this.inputNoValidation.setInputValue('diiirty')
    this.inputNoValidation.triggerEvent('blur')

    this.assert.dom(this.inputNoValidation.element).hasClass('is-dirty')
    this.assert.dom(this.inputNoValidation.element).lacksClass('is-pristine')
  }

  'test Input#blur should not apply is-dirty class name when a value has not been entered'() {
    this.inputNoValidation.triggerEvent('blur')

    this.assert.dom(this.inputNoValidation.element).hasClass('is-pristine')
    this.assert.dom(this.inputNoValidation.element).lacksClass('is-dirty')
  }

  'test Input#blur should apply is-valid class name when a value has been entered to a field with no validation'() {
    this.inputNoValidation.setInputValue('diiirty')
    this.inputNoValidation.triggerEvent('blur')

    this.assert.dom(this.inputNoValidation.element).hasClass('is-valid')
  }

  'test Input#blur should not apply is-invalid class name when a value has been entered to a field with no validation'() {
    this.inputNoValidation.setInputValue('diiirty')
    this.inputNoValidation.triggerEvent('blur')

    this.assert.dom(this.inputNoValidation.element).lacksClass('is-invalid')
  }

  'test Input#blur should not apply is-valid class name when a value has not been entered to a field with no validation'() {
    this.inputNoValidation.triggerEvent('blur')

    this.assert.dom(this.inputNoValidation.element).lacksClass('is-valid')
  }

  'test Input#blur should not apply is-invalid class name when a value has not been entered to a field with no validation'() {
    this.inputNoValidation.triggerEvent('blur')

    this.assert.dom(this.inputNoValidation.element).lacksClass('is-invalid')
  }

  'test Input#blur should apply is-valid class name when a value has been entered to a field with validation'() {
    this.inputWithValidation.setInputValue('diiirty')
    this.inputWithValidation.triggerEvent('blur')

    this.assert.dom(this.inputWithValidation.element).hasClass('is-valid')
  }

  'test Input#blur should apply is-invalid class name when a value has been entered to a field with validation'() {
    this.inputWithValidation.setInputValue('diiirty')
    this.inputWithValidation.triggerEvent('blur')

    this.assert.dom(this.inputWithValidation.element).lacksClass('is-invalid')
  }

  'test Input#blur should not apply is-valid class name when a value has not been entered to a field with validation'() {
    this.inputWithValidation.triggerEvent('blur')

    this.assert.dom(this.inputWithValidation.element).lacksClass('is-valid')
  }

  'test Input#blur should not apply is-invalid class name when a value has not been entered to a field with validation'() {
    this.inputWithValidation.triggerEvent('blur')

    this.assert.dom(this.inputWithValidation.element).lacksClass('is-invalid')
  }

  get inputWithValidation() {
    return new InputElement(this,'withValidation')
  }

  get inputNoValidation() {
    return new InputElement(this,'noValidation')
  }
}
