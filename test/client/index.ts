import 'babel-polyfill'
const context = require.context('.', true, /\.spec\.ts$/)
const modules = context.keys().map(key => context(key).default)
modules.forEach(constructor => constructor.defineModule())