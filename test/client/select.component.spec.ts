import {ApplicationTestCase} from '@stimulus/test'
import '../support/qunit.assertions'

import {SelectController} from '../../lib/assets/components/vf-select'

export default class SelectTests extends ApplicationTestCase {

  fixtureHTML = `
    <select name="noValidation"
            data-controller="select"
            data-action="focus->select#focus blur->select#blur">
      <option disabled selected>Select</option>
      <option>1</option>
    </select>
    <select name="withValidation"
            data-controller="select"
            data-action="focus->select#focus blur->select#blur"
            required>
      <option disabled selected>Select</option>
      <option>1</option>
    </select>
    `

  setupApplication() {
    this.application.register('select', SelectController)
  }

  'test Select#connect should apply is-pristine class name'() {
    this.assert.dom(this.selectNoValidation).hasClass('is-pristine')
  }

  'test Select#focus should apply is-focused class name'() {
    this.triggerEvent(this.selectNoValidation,'focus')

    this.assert.dom(this.selectNoValidation).hasClass('is-focused')
  }

  'test Select#blur should remove is-focused class name'() {
    this.triggerEvent(this.selectNoValidation, 'focus')
    this.triggerEvent(this.selectNoValidation, 'blur')

    this.assert.dom(this.selectNoValidation).lacksClass('is-focused')
  }

  'test Select#blur should apply is-touched class name'() {
    this.triggerEvent(this.selectNoValidation, 'blur')

    this.assert.dom(this.selectNoValidation).hasClass('is-touched')
  }

  'test Select#blur should apply is-dirty class name when a value has been entered'() {
    this.selectNoValidation.value = '1'
    this.triggerEvent(this.selectNoValidation, 'blur')

    this.assert.dom(this.selectNoValidation).hasClass('is-dirty')
    this.assert.dom(this.selectNoValidation).lacksClass('is-pristine')
  }

  'test Select#blur should not apply is-dirty class name when a value has not been entered'() {
    this.triggerEvent(this.selectNoValidation, 'blur')

    this.assert.dom(this.selectNoValidation).hasClass('is-pristine')
    this.assert.dom(this.selectNoValidation).lacksClass('is-dirty')
  }

  'test Select#blur should apply is-valid class name when a value has been entered to a field with no validation'() {
    this.selectNoValidation.value = '1'
    this.triggerEvent(this.selectNoValidation, 'blur')

    this.assert.dom(this.selectNoValidation).hasClass('is-valid')
  }

  'test Select#blur should not apply is-invalid class name when a value has been entered to a field with no validation'() {
    this.selectNoValidation.value = '1'
    this.triggerEvent(this.selectNoValidation, 'blur')

    this.assert.dom(this.selectNoValidation).lacksClass('is-invalid')
  }

  'test Select#blur should not apply is-valid class name when a value has not been entered to a field with no validation'() {
    this.triggerEvent(this.selectNoValidation, 'blur')

    this.assert.dom(this.selectNoValidation).lacksClass('is-valid')
  }

  'test Select#blur should not apply is-invalid class name when a value has not been entered to a field with no validation'() {
    this.triggerEvent(this.selectNoValidation, 'blur')

    this.assert.dom(this.selectNoValidation).lacksClass('is-invalid')
  }

  'test Select#blur should apply is-valid class name when a value has been entered to a field with validation'() {
    this.selectWithValidation.value = '1'
    this.triggerEvent(this.selectWithValidation, 'blur')

    this.assert.dom(this.selectWithValidation).hasClass('is-valid')
  }

  'test Select#blur should apply is-invalid class name when a value has been entered to a field with validation'() {
    this.selectWithValidation.value = '1'
    this.triggerEvent(this.selectWithValidation, 'blur')

    this.assert.dom(this.selectWithValidation).lacksClass('is-invalid')
  }

  'test Select#blur should not apply is-valid class name when a value has not been entered to a field with validation'() {
    this.triggerEvent(this.selectWithValidation, 'blur')

    this.assert.dom(this.selectWithValidation).lacksClass('is-valid')
  }

  'test Select#blur should not apply is-invalid class name when a value has not been entered to a field with validation'() {
    this.triggerEvent(this.selectWithValidation, 'blur')

    this.assert.dom(this.selectWithValidation).lacksClass('is-invalid')
  }

  get selectWithValidation() {
    return this.selectEl('withValidation')
  }

  get selectNoValidation() {
    return this.selectEl('noValidation')
  }

  selectEl(name) {
    return this.findElement(`select[name=${name}]`) as HTMLSelectElement
  }
}
