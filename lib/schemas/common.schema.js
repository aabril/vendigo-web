'use strict'

const joi = require('joi')

exports.isoDate = function() {
  return joi
    .string()
    .trim()
    .isoDate()
}

exports.strMax = function(max) {
  return joi
    .string()
    .trim()
    .max(max)
}

exports.uuid = function() {
  return joi
    .string()
    .trim()
    .guid()
}
