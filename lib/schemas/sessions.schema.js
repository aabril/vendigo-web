'use strict'

const joi = require('joi')
const cs = require('schemas/common.schema')

exports.payload = function() {
  return {
    email: cs
      .strMax(255)
      .email()
      .required(),
    password: joi
      .string()
      .max(255)
      .required()
  }
}
