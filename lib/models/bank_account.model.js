module.exports = (sequelize, DataTypes) => {
  const model = {
    name: 'BankAccount',
    schema: {
      author: DataTypes.STRING
    },
    config: {}
  }

  return sequelize.define(model.name, model.schema, model.config)
}
