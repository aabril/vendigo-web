const bcrypt = 'bcrypt'

module.exports = (sequelize, DataTypes) => {
  const model = {
    name: 'users',
    schema: {
      firstName: {
        type: DataTypes.STRING(50),
        allowNull: false,
        validate: {
          len: {
            args: [0, 50],
            msg: 'First Name is too long'
          }
        }
      },
      lastName: {
        type: DataTypes.STRING(100),
        allowNull: false,
        validate: {
          len: {
            args: [0, 100],
            msg: 'Last Name is too long'
          }
        }
      },
      email: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: true,
        validate: {
          isEmail: {
            msg: 'Is not a valid email address'
          },
          isUnique: sequelize.validateIsUnique(
            'email',
            'This email already exists in the system.'
          )
        }
      },
      password_hash: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    config: {
      instanceMethods: {
        generateHash: function(password_hash) {
          return bcrypt.hashSync(password_hash, bcrypt.genSaltSync(8), null)
        },
        validPassword: function(password_hash) {
          return bcrypt.compareSync(password_hash, this.password_hash)
        }
      }
    }
  }

  return sequelize.define(model.name, model.schema, model.config)
}
