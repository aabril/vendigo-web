module.exports = (sequelize, DataTypes) => {
  const model = {
    name: 'Applicant',
    schema: {
      author: DataTypes.STRING
    },
    config: {}
  }

  return sequelize.define(model.name, model.schema, model.config)
}
