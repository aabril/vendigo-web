module.exports = (sequelize, DataTypes) => {
  const model = {
    name: 'AccountTransaction',
    schema: {
      author: DataTypes.STRING
    },
    config: {}
  }

  return sequelize.define(model.name, model.schema, model.config)
}
