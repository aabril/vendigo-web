'use strict'

const {includes} = require('lodash')

module.exports = function(collection, val, {fn, hash, inverse}) {
  if (includes(collection, val, hash.fromIndex || 0)) {
    return fn(this)
  } else {
    return inverse(this)
  }
}
