'use strict'

module.exports = function(valA, valB, {fn, inverse}) {
  if (valA === valB) {
    return fn(this)
  } else {
    return inverse(this)
  }
}
