'use strict'

const {pick} = require('lodash')

const PARENT_ATTRS = ['disabled', 'readonly']

const INPUT_ATTRS = [
  'accept',
  'autocomplete',
  'autofocus',
  'capture',
  'checked',
  'disabled',
  'form',
  'formaction',
  'formenctype',
  'formmethod',
  'formnovalidate',
  'height',
  'id',
  'list',
  'max',
  'maxlength',
  'min',
  'minlength',
  'multiple',
  'name',
  'pattern',
  'placeholder',
  'readonly',
  'required',
  'selectionDirection',
  'selectionEnd',
  'selectionStart',
  'size',
  'spellcheck',
  'src',
  'step',
  'tabindex',
  'type',
  'value',
  'width'
]

const SPACE = ' '

const DEFAULTS = {
  class: () => ['vf-input'],
  controller: () => ['input'],
  action: () => ['focus->input#focus blur->input#blur'],
  target: ({name}) => [`form.${name} input.input`]
}

function getAttributesForPropName(propName, hash) {
  const defaultAttrsForPropName = DEFAULTS[propName](hash)
  const clonedAttrs = [...defaultAttrsForPropName]

  if (hash[propName]) {
    clonedAttrs.push(hash[propName])
  }

  return clonedAttrs.join(SPACE)
}

function getAttributes(hash, attrs) {
  const iter = (key) =>
    isBooleanAttribute(key) ? key : `${key}="${hash[key]}"`
  return pickAttrs(hash, attrs)
    .map(iter)
    .join(SPACE)
}

function isBooleanAttribute(attr) {
  return attr === 'disabled' || attr === 'readonly'
}

function pickAttrs(hash, attrs) {
  return Object.keys(pick(hash, attrs))
}

module.exports = function({hash}) {
  let prepend = ''
  let append = ''

  if (!hash.name) {
    throw '{{vf-input}}: a name attribute must be supplied'
  }

  if (hash.prepend) {
    prepend = `<div class="vf-input__prepend"><span>${
      hash.prepend
    }</span></div>`
  }

  if (hash.append) {
    append = `<div class="vf-input__append"><span>${hash.append}</span></div>`
  }

  return `<div class="${getAttributesForPropName('class', hash)}"
               data-controller="${getAttributesForPropName('controller', hash)}"
               data-target="input.wrap"
               ${getAttributes(hash, PARENT_ATTRS)}>
            ${prepend}
            <input class="vf-input__input"
                   ${getAttributes(hash, INPUT_ATTRS)}
                   data-target="${getAttributesForPropName('target', hash)}"
                   data-action="${getAttributesForPropName('action', hash)}" />
            ${append}
          </div>`
}
