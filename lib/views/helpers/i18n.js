'use strict'

const {get, isString, isObject, isUndefined} = require('lodash')

module.exports = function(key, {hash = {}} = {}) {
  const language = isObject(hash.language) ? hash.language : this.language

  if (!isString(key)) {
    throw '{{i18n}} helper: invalid key. Object keys must be formatted as strings.'
  }

  if (isUndefined(language)) {
    throw '{{i18n}} helper: the "language" parameter is not defined.'
  }

  if (isUndefined(get(language, key))) {
    throw `{{i18n}} helper: property "${key}" is not defined for language.`
  }

  return get(language, key)
}
