'use strict'

const {kebabCase} = require('lodash')

module.exports = function({name}) {
  return `${kebabCase(name)}-fields`
}
