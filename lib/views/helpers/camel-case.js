'use strict'

const {camelCase} = require('lodash')

module.exports = function(val) {
  return camelCase(val)
}
