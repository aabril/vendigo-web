'use strict'

const {kebabCase} = require('lodash')

module.exports = function(val) {
  return kebabCase(val)
}
