'use strict'

const {isObject, isString} = require('lodash')

const EMPTY_STRING = ''
const SEPARATOR = EMPTY_STRING

function getOptionsFromArgs(args) {
  return args.find((arg) => isObject(arg) && arg.hash) || {}
}

module.exports = function(...args) {
  const {hash = {}} = getOptionsFromArgs(args)
  const separator = hash.separator || SEPARATOR

  return args.reduce(
    (prev, curr) =>
      isString(curr) ? `${prev}${separator}${curr}` : `${prev}${EMPTY_STRING}`
  )
}
