'use strict'

const {snakeCase} = require('lodash')

module.exports = function(val) {
  return snakeCase(val)
}
