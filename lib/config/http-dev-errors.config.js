function httpDevErrors() {
  return {
    plugin: require('hapi-dev-errors'),
    options: {
      showErrors: process.env.NODE_ENV !== 'production'
    }
  }
}

module.exports = httpDevErrors
