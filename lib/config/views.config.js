'use strict'

const path = require('path')
const handlebars = require('handlebars')
const configService = require('services/config.service')
const envService = require('services/env.service')
const sessionService = require('services/session.service')

function viewsConfig() {
  return {
    context: function(request) {
      const locale = configService.get('LOCALE')
      const context = {
        language: require(`assets/translations/${locale}.translation`),
        isAuthenticated: request.auth.isAuthenticated,
        isProduction: envService.isProduction(),
        user: sessionService.userFromState(request.state)
      }

      if (path.extname(request.url.path) === '') {
        context.section = request.url.path.split('/')[1]
      }

      return context
    },
    engines: {
      hbs: handlebars
    },
    helpersPath: path.resolve('lib', 'views', 'helpers'),
    layout: 'default',
    layoutPath: path.resolve('lib', 'views', 'layout'),
    partialsPath: path.resolve('lib', 'views', 'partials'),
    path: path.resolve('lib', 'views')
  }
}

module.exports = viewsConfig
