'use strict'

function essentialPlugins() {
  return [require('hapi-auth-cookie'), require('vision'), require('inert')]
}

module.exports = essentialPlugins
