'use strict'

// local
const configService = require('services/config.service')
const envService = require('services/env.service')

module.exports = {
  password: configService.get('PRIVATE:KEY'),
  isSecure: envService.isProduction(),
  redirectTo: '/login',
  cookie: 'sid',
  ttl: 24 * 60 * 60 * 1000
}
