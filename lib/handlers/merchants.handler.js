'use strict'

const merchantsRepository = require('repositories/merchants.repository')
const loggerService = require('services/logger.service')

exports.all = async function(request, h) {
  try {
    const merchants = await merchantsRepository.all()
    return h.view('merchants/listing', {merchants})
  } catch (err) {
    loggerService.error(['merchants'], err)
    throw err
  }
}

exports.get = async function(request, h) {
  try {
    const {uuid} = request.params
    const merchant = await merchantsRepository.get(uuid)
    return h.view('merchants/show', {merchant})
  } catch (err) {
    loggerService.error(['merchants'], err)
    throw err
  }
}

exports.new = function(request, h) {
  return h.view('merchants/new', {})
}

exports.create = async function(request, h) {
  try {
    const {merchant} = request.params
    const {uuid} = await merchantsRepository.create(merchant)
    return h.redirect(`/merchants/${uuid}`)
  } catch (err) {
    loggerService.error(['merchants'], err)
    throw err
  }
}
