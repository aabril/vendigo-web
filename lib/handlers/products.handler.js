'use strict'

const loggerService = require('services/logger.service')

exports.fields = async function(request, h) {
  try {
    const {categoryName: name} = request.params
    return h.view(
      'products/category-fields',
      {currentProduct: {name}},
      {layout: false}
    )
  } catch (err) {
    loggerService.error(['applications'], err)
    throw err
  }
}
