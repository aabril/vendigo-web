'use strict'

module.exports = function(request, h) {
  return h.redirect('/login').code(307)
}
