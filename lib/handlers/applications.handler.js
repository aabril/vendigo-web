'use strict'

const applicationsRepository = require('repositories/applications.repository')
const loggerService = require('services/logger.service')

exports.all = async function(request, h) {
  try {
    const applicationItems = await applicationsRepository.all()
    return h.view('applications/listing', {applicationItems})
  } catch (err) {
    loggerService.error(['applications'], err)
    throw err
  }
}

exports.get = async function(request, h) {
  try {
    const {uuid} = request.params
    const application = await applicationsRepository.get(uuid)
    return h.view('applications/show', {application})
  } catch (err) {
    loggerService.error(['applications'], err)
    throw err
  }
}

exports.new = function(request, h) {
  const verticals = [
    {id: 1, name: 'Kitchen'},
    {id: 2, name: 'Carpentry'},
    {id: 3, name: 'Electrical'},
    {id: 4, name: 'Plumbing'},
    {id: 5, name: 'Painting'},
    {id: 6, name: 'Boiler installation'},
    {id: 7, name: 'Masonry'},
    {id: 8, name: 'Gardening'},
    {id: 9, name: 'Glaziers'},
    {id: 10, name: 'Fenestration'},
    {id: 11, name: 'Other'}
  ]
  const productCategories = [
    {id: 1, name: 'Interest free'},
    {id: 2, name: 'Buy now, pay later'},
    {id: 3, name: 'Interest bearing'}
  ]
  const terms = [6, 9, 10, 12, 24, 36, 48, 60, 84, 120]

  return h.view('applications/new', {
    step: 'payment-terms',
    currentProduct: productCategories[0],
    verticals,
    productCategories,
    terms
  })
}

exports.create = async function(request, h) {
  try {
    const {application} = request.params
    const {uuid} = await applicationsRepository.create(application)
    return h.redirect(`/applications/${uuid}/details`)
  } catch (err) {
    loggerService.error(['applications'], err)
    throw err
  }
}

exports.edit = function(template) {
  return async function(request, h) {
    try {
      const {uuid} = request.params
      const application = await applicationsRepository.get(uuid)
      return h.view(`applications/edit-${template}`, {application})
    } catch (err) {
      loggerService.error(['applications'], err)
      throw err
    }
  }
}

exports.update = async function(request, h) {
  try {
    const {application, target} = request.params
    await applicationsRepository.update(application)
    return h.redirect(`/applications/${application.uuid}/${target}`)
  } catch (err) {
    loggerService.error(['applications'], err)
    throw err
  }
}
