'use strict'

const loggerService = require('services/logger.service')
const authService = require('services/auth.service')

exports.new = function(request, h) {
  if (request.auth.isAuthenticated) {
    return h.redirect('/applications')
  }
  return h.view('sessions/new', {})
}

exports.createValidationError = async function(request, h, error) {
  let messages
  if (error && error.details) {
    messages = error.details.map((e) => e.message)
  }

  return h
    .view('sessions/new', {messages})
    .code(200)
    .takeover()
}

exports.create = async function(request, h) {
  const {email, password} = request.payload

  try {
    const user = await authService.authenticate(email, password)
    request.cookieAuth.set({
      user: user.email,
      role: user.role,
      lastLogin: new Date()
    })
    return h.redirect('/applications')
  } catch (err) {
    loggerService.error(err)
    request.cookieAuth.clear()
    return h.view('sessions/new', {messages: ['Invalid email or password']})
  }
}

exports.destroy = function(request, h) {
  request.cookieAuth.clear()
  return h.redirect('/login')
}
