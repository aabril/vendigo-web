'use strict'

exports.isDevelopment = function() {
  return isEnv('development')
}

exports.isProduction = function() {
  return isEnv('production')
}

exports.isQa = function() {
  return isEnv('qa')
}

exports.isTest = function() {
  return isEnv('test')
}

function isEnv(env) {
  return process.env['NODE_ENV'] === env
}
