'use strict'

const authService = jest.genMockFromModule('services/auth.service')

let mockUsers = Object.create(null)

function __setMockUser(user) {
  mockUsers[user.email] = user
}

function __resetMockUsers() {
  mockUsers = Object.create(null)
}

function authenticate(username) {
  const user = mockUsers[username]

  if (!user) {
    throw new Error('User not found')
  }

  return user
}

authService.__setMockUser = __setMockUser
authService.__resetMockUsers = __resetMockUsers
authService.authenticate = authenticate

module.exports = authService
