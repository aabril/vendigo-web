/* eslint no-console: "off" */
'use strict'

const configService = require('services/config.service')
const log = require('console-log-level')({
  level: configService.get('LOG:LEVEL')
})
const {info, warning, error} = require('log-symbols')

exports.debug = function(datum) {
  console.dir(datum, {showHidden: true, depth: 2, colors: true})
}

exports.error = function(...args) {
  const {tags, message} = createLog('error', args)
  log.error(error, tags, message)
  log.error(arguments[1])
}

exports.info = function(...args) {
  const {tags, message} = createLog('info', args)
  log.info(info, tags, message)
}

exports.warn = function(...args) {
  const {tags, message} = createLog('warn', args)
  log.warn(warning, tags, message)
}

function createLog(type, args) {
  if (Array.isArray(args[0]) && args.length > 1) {
    const tags = args[0].concat(type)
    return {
      tags,
      message: args[1]
    }
  } else {
    return {
      tags: [type],
      message: args[0]
    }
  }
}
