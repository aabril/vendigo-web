'use strict'

const dotenv = require('dotenv')
dotenv.config()

process.env.NODE_ENV = process.env.NODE_ENV || 'development'
process.env.PORT = process.env.PORT || 3000
process.env.PRIVATE_KEY =
  process.env.PRIVATE_KEY || 'keyboardcatkeyboardcatkeyboardcat'
process.env.LOCALE = process.env.LOCALE || 'en-gb'

exports.get = function(key) {
  return process.env[key.toString().replace(/:/g, '_')]
}
