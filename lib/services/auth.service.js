'use strict'

const bcrypt = require('bcrypt')

const usersRepository = require('repositories/users.repository')

exports.authenticate = async function(email, password) {
  const user = await usersRepository.getByEmail(email)
  if (user) {
    const isMatch = await bcrypt.compare(password, user.password_hash)
    if (isMatch) {
      return serializeUser(user)
    } else {
      throw new Error('User not found')
    }
  } else {
    throw new Error('User not found')
  }
}

function serializeUser(user) {
  const cloneUser = JSON.parse(JSON.stringify(user))
  delete cloneUser.passwordHash
  return cloneUser
}
