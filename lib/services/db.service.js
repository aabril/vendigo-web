'use strict'

const Sequelize = require('sequelize')
require('sequelize-isunique-validator')(Sequelize)

const fs = require('fs')
const path = require('path')
const basename = path.basename(module.filename)
const dbConfig = require('config/db.config')

const db = {}

function getSequelizeInstance(dbname, username, password) {
  const config = {
    dialect: 'postgres',
    operatorsAliases: false
  }
  return new Sequelize(dbname, username, password, config)
}

function addModelsToDbModule(sequelize) {
  const modelsPath = path.join(__dirname, '../models/')
  fs
    .readdirSync(modelsPath)
    .filter(
      (file) =>
        file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js'
    )
    .forEach((file) => {
      const model = sequelize.import(path.join(modelsPath, file))
      db[model.name] = model
    })

  Object.keys(db).forEach((modelName) => {
    if (db[modelName].associate) {
      db[modelName].associate(db)
    }
  })
}

function initialise() {
  const sequelize = getSequelizeInstance(
    dbConfig.database,
    dbConfig.username,
    dbConfig.password
  )

  addModelsToDbModule(sequelize)

  db.sequelize = sequelize
  db.Sequelize = Sequelize
}

db.initialise = initialise
module.exports = db
