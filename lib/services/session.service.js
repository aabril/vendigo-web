'use strict'

exports.userFromState = function(state) {
  const {sid: user = {}} = state || {}
  return hasKeys(user)
    ? {
        email: user.email,
        isAdmin: user.role === 'admin',
        isCustomer: user.role === 'customer',
        isMerchant: user.role === 'merchant'
      }
    : undefined
}

function hasKeys(obj) {
  return Object.keys(obj || {}).length
}
