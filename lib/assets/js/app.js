import 'babel-polyfill'
import {Application} from 'stimulus'

import '../styles/main.styl'

import {FormController} from '../components/vf-form'
import {InputController} from '../components/vf-input'
import {SelectController} from '../components/vf-select'
import {PaymentTermsFormController} from '../components/payment-terms-form'
import {ProductCategoryController} from './controllers/product-category.controller'

const application = Application.start()

application.register('form', FormController)
application.register('input', InputController)
application.register('select', SelectController)
application.register('payment-terms-form', PaymentTermsFormController)
application.register('product-category', ProductCategoryController)
