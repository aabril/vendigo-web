import {kebabCase} from 'lodash'
import {Controller} from 'stimulus'

import {fetchTemplate} from '../services/product-category-template.service'

class ProductCategoryController extends Controller {
  static get targets() {
    return ['fields']
  }

  async change() {
    const {id} = this.element.querySelector('input:checked')
    const categoryName = kebabCase(id)

    try {
      this.fieldsTarget.innerHTML = await fetchTemplate(categoryName)
    } catch (ex) {
      // log error
    }
  }
}

export {ProductCategoryController}
