import {Controller} from 'stimulus'

const IS_DIRTY = 'is-dirty'
const IS_FOCUSED = 'is-focused'
const IS_INVALID = 'is-invalid'
const IS_PRISTINE = 'is-pristine'
const IS_TOUCHED = 'is-touched'
const IS_VALID = 'is-valid'

export default class extends Controller {
  connect() {
    this.element.classList.add(IS_PRISTINE)
  }

  focus() {
    this.element.classList.add(IS_FOCUSED)
  }

  blur() {
    const classListToAdd = [IS_TOUCHED]
    const classListToRemove = [IS_FOCUSED]

    if (this.isDirty) {
      classListToAdd.push(IS_DIRTY)
      classListToRemove.push(IS_PRISTINE)
    }

    if (this.shouldValidate) {
      if (this.isValid) {
        classListToAdd.push(IS_VALID)
        classListToRemove.push(IS_INVALID)
      } else {
        classListToAdd.push(IS_INVALID)
        classListToRemove.push(IS_VALID)
      }
    }

    this.element.classList.add(...classListToAdd)
    this.element.classList.remove(...classListToRemove)
  }

  get isValid() {
    return this.formControl.checkValidity()
  }

  get shouldValidate() {
    return this.isDirty
  }
}
