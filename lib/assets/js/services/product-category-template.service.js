import {$http} from './$http.service'

function fetchTemplate(name) {
  return $http(`/products/categories/fields/${name}`).then((response) =>
    response.text()
  )
}

export {fetchTemplate}
