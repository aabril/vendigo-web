const DEFAULT_$HTTP_CONFIG = {
  credentials: 'same-origin'
}

function $http(url, conf = {}) {
  conf = Object.assign(DEFAULT_$HTTP_CONFIG, conf)

  return fetch(url, conf).then((response) => {
    if (response.status > 299) {
      return Promise.reject(response)
    }
    return response
  })
}

export {$http}
