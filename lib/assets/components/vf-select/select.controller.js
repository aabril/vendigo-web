import FormControl from '../../js/controllers/form-control.controller'

export default class extends FormControl {
  get formControl() {
    return this.element
  }

  get isDirty() {
    const selectedOptions = Array.from(this.formControl.selectedOptions).filter(
      ({disabled}) => !disabled
    )
    return Boolean(selectedOptions.length)
  }
}
