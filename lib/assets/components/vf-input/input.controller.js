import {get} from 'lodash'
import FormControl from '../../js/controllers/form-control.controller'

export default class extends FormControl {
  static get targets() {
    return ['input']
  }

  get formControl() {
    return this.inputTarget
  }

  get isDirty() {
    return Boolean(get(this.formControl, 'value', '').length)
  }
}
