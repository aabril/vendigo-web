import {Controller} from 'stimulus'

export default class extends Controller {
  static get targets() {
    return ['deposit', 'depositPercentage', 'purchaseAmount']
  }

  updateDeposit() {
    const purchaseAmount = this.purchaseAmountTarget.value
    const depositPercentage = this.depositPercentageTarget.value

    this.depositTarget.value = (
      depositPercentage /
      100 *
      purchaseAmount
    ).toFixed()
  }
}
