'use strict'

exports.all = function() {
  return []
}

exports.get = function(uuid) {
  return {uuid}
}

exports.create = function(application) {
  return {application}
}

exports.update = function(application) {
  return {application}
}
