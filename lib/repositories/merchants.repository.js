'use strict'

exports.all = function() {
  return []
}

exports.get = function(uuid) {
  return {uuid}
}

exports.create = function(merchant) {
  return {merchant}
}
