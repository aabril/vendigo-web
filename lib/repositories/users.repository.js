'use strict'

const dbService = require('services/db.service')

exports.getByEmail = async function(email) {
  const {users} = dbService

  const result = await users.findOne({
    attributes: ['email', 'password_hash'],
    where: {email}
  })

  return result ? result.dataValues : undefined
}
