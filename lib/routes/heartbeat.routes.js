'use strict'

// local
const heartbeatHandler = require('handlers/heartbeat.handler')

module.exports = [
  {
    method: 'GET',
    path: '/heartbeat',
    options: {
      auth: false,
      description: 'Check server health',
      handler: heartbeatHandler
    }
  }
]
