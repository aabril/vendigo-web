'use strict'

const merchantsHandler = require('handlers/merchants.handler')

module.exports = [
  {
    method: 'GET',
    path: '/merchants',
    options: {
      description: 'Display merchants listing page',
      handler: merchantsHandler.all
    }
  },
  {
    method: 'POST',
    path: '/merchants',
    options: {
      description: 'Create new merchant',
      handler: merchantsHandler.create
    }
  },
  {
    method: 'GET',
    path: '/merchants/new',
    options: {
      description: 'Display a add a merchant page',
      handler: merchantsHandler.new
    }
  },
  {
    method: 'GET',
    path: '/merchants/{uuid}',
    options: {
      description: 'Display merchant by uuid',
      handler: merchantsHandler.get
    }
  }
]
