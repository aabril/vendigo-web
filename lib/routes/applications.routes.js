'use strict'

const applicationsHandler = require('handlers/applications.handler')

module.exports = [
  {
    method: 'GET',
    path: '/applications',
    options: {
      description: 'Display loan applications listing page',
      handler: applicationsHandler.all
    }
  },
  {
    method: 'POST',
    path: '/applications',
    options: {
      description: 'Create new loan application',
      handler: applicationsHandler.create
    }
  },
  {
    method: 'GET',
    path: '/applications/new',
    options: {
      description: 'Display new loan application page',
      handler: applicationsHandler.new
    }
  },
  {
    method: 'PUT',
    path: '/applications/{uuid}',
    options: {
      description: 'Update loan application',
      handler: applicationsHandler.update
    }
  },
  {
    method: 'GET',
    path: '/applications/{uuid}',
    options: {
      description: 'Display loan application by uuid',
      handler: applicationsHandler.get
    }
  },
  {
    method: 'GET',
    path: '/applications/{uuid}/details',
    options: {
      description: 'Display about customer details page',
      handler: applicationsHandler.edit('details')
    }
  },
  {
    method: 'GET',
    path: '/applications/{uuid}/invite',
    options: {
      description: 'Display invite customer page',
      handler: applicationsHandler.edit('invite')
    }
  },
  {
    method: 'GET',
    path: '/applications/{uuid}/processing',
    options: {
      description: 'Display spinner and wait for lender response',
      handler: applicationsHandler.edit('processing')
    }
  },
  {
    method: 'GET',
    path: '/applications/{uuid}/documents',
    options: {
      description: 'Display upload documents page',
      handler: applicationsHandler.edit('documents')
    }
  },
  {
    method: 'GET',
    path: '/applications/{uuid}/approved',
    options: {
      description: 'Display upload documents page',
      handler: applicationsHandler.edit('approved')
    }
  },
  {
    method: 'GET',
    path: '/applications/{uuid}/declined',
    options: {
      description: 'Display declined page',
      handler: applicationsHandler.edit('declined')
    }
  },
  {
    method: 'GET',
    path: '/applications/{uuid}/accepted',
    options: {
      description: 'Display declined page',
      handler: applicationsHandler.edit('accepted')
    }
  }
]
