'use strict'

// local
const homeHandler = require('handlers/home.handler')

module.exports = [
  {
    method: 'GET',
    path: '/',
    options: {
      auth: {
        mode: 'try'
      },
      description: 'Redirect to loan applications listing page',
      handler: homeHandler
    }
  }
]
