'use strict'

const fs = require('fs')

let result = []

fs.readdirSync(__dirname).forEach((filename) => {
  if (Array.isArray(filename.match(/\.routes/))) {
    result = result.concat(require(`./${filename}`))
  }
})

module.exports = result
