'use strict'

const sessionsHandler = require('handlers/sessions.handler')
const sessionsSchema = require('schemas/sessions.schema')

module.exports = [
  {
    method: 'GET',
    path: '/login',
    options: {
      auth: {
        mode: 'try'
      },
      plugins: {'hapi-auth-cookie': {redirectTo: false}},
      description: 'Display login page',
      handler: sessionsHandler.new
    }
  },
  {
    method: 'POST',
    path: '/login',
    options: {
      auth: false,
      description: 'Create user session',
      handler: sessionsHandler.create,
      validate: {
        payload: sessionsSchema.payload(),
        failAction: sessionsHandler.createValidationError
      }
    }
  },
  {
    method: 'GET',
    path: '/logout',
    options: {
      description: 'Destroy user session',
      handler: sessionsHandler.destroy
    }
  }
]
