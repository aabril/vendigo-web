'use strict'

const productsHandler = require('handlers/products.handler')

module.exports = [
  {
    method: 'GET',
    path: '/products/categories/fields/{categoryName}',
    options: {
      description: 'Returns fields required for a product category',
      handler: productsHandler.fields
    }
  }
]
