'use strict'

module.exports = [
  {
    method: 'GET',
    path: '/static/{param*}',
    options: {
      auth: false,
      description: 'Access static assets',
      handler: {
        directory: {
          path: 'public'
        }
      }
    }
  }
]
