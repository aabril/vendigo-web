'use strict'

module.exports = {
  type: 'onPreResponse',
  method({response}, h) {
    if (is404Page()) {
      return h.view('404', null, {layout: false}).code(404)
    }
    if (is500Page()) {
      return h
        .view(
          '500',
          {
            message: response.message,
            stack: response.stack
          },
          {
            layout: false
          }
        )
        .code(500)
    }
    return h.continue

    function is404Page() {
      return response.isBoom && response.output.statusCode === 404
    }

    function is500Page() {
      return response.isBoom && response.isServer
    }
  }
}
