'use strict'

const NormalModuleReplacementPlugin = require('webpack')
  .NormalModuleReplacementPlugin

const config = {
  browsers: ['ChromeHeadless'],

  frameworks: ['qunit'],

  reporters: ['progress'],

  singleRun: true,

  autoWatch: false,

  files: [{pattern: 'test/client/index.ts'}],

  preprocessors: {
    'test/**/*.+(js|ts)': ['webpack'],
    'lib/**/*.js': ['webpack']
  },

  mime: {
    'text/x-typescript': ['ts']
  },

  webpack: {
    devtool: 'inline-source-map',

    module: {
      rules: [
        {
          test: /\.ts$/,
          use: [{loader: 'ts-loader'}]
        },
        {
          test: /\.js$/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['babel-preset-env'],
                plugins: [require('babel-plugin-transform-class-properties')]
              }
            }
          ]
        }
      ]
    },

    resolve: {
      extensions: ['.ts', '.js']
    },

    plugins: [
      new NormalModuleReplacementPlugin(
        /\.(jpe?g|gif|png|styl|css|woff2?|ttf|eot|svg)$/,
        'node-noop'
      )
    ]
  },

  webpackMiddleware: {
    noInfo: true
  },

  client: {
    clearContext: false,
    qunit: {
      showUI: true
    }
  },

  captureTimeout: 180000,
  browserDisconnectTimeout: 180000,
  browserDisconnectTolerance: 3,
  browserNoActivityTimeout: 300000
}

module.exports = function(karmaConfig) {
  karmaConfig.set(config)
}
