'use strict'

const assert = require('assert')

const loggerService = require('services/logger.service')
const {initServer, stopServer} = require('./server')

process.on('SIGINT', async () => {
  await stopServer()
})

process.on('unhandledRejection', (err) => {
  loggerService.error(`Unhandled Promise Error:\n`, err)
})

initServer().then(async (server) => {
  try {
    await server.start()
    loggerService.info(
      `Server started at: ${server.info.uri.toLowerCase()} in ${
        process.env.NODE_ENV
      } environment`
    )
  } catch (err) {
    loggerService.error(`Server failed to start: ${err}`)
    assert(!err, err)
  }
})
