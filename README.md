# vendigo-web

The Vendigo Finance POS lending platform web application.

## Migrations

To run the migrations use the following command.

    $ db-migrate up

For more information see the following resource https://db-migrate.readthedocs.io/en/latest/

## Versioning

When tagging a new release use npm. It will update the version in the `package.json` file before committing the change to git and adding the appropriate git tag.

To release a **bugfix** update the **patch version**.

    $ npm version patch
    $ git push
    $ git push --tags

To release a **feature** update the **minor version**.

    $ npm version minor
    $ git push
    $ git push --tags

To release a **breaking change** update the **major version**.

    $ npm version major
    $ git push
    $ git push --tags

## License

Copyright © 2018 Vendigo Finance Regional Limited. All rights reserved.
