FROM node:9.5-alpine

RUN mkdir -p /opt/app
WORKDIR /opt/app

COPY ./package.json /opt/app

RUN apk add --update --no-cache tini &&\
  apk add --virtual .build-dependencies make gcc g++ python &&\
  npm install --production &&\
  npm cache clean --force &&\
  apk del .build-dependencies

COPY . /opt/app

ENV NODE_ENV=production NODE_PATH=./lib

EXPOSE 8080

USER node

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["node", "index.js"]
